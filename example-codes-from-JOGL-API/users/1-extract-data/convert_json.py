#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import sys
import pandas as pd

table = pd.read_json('users_jogl.json', encoding='utf8')  

table.to_csv('users_jogl.tsv', sep='\t', index=False)
