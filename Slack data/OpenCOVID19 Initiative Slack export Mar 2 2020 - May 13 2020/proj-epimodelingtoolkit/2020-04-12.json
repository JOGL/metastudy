[
    {
        "client_msg_id": "fac2541b-44df-421b-8d60-33615c54448d",
        "type": "message",
        "text": "Hey all. Have been noticing that a lot of media attention in the US is about the concept of 'peaks' and trying to forecast from data whether specific regions (Washington, NY, France, Spain) are actually experiencing them. I put together a quick simulation based analysis of how changes to the effective reproductive number can result in temporary plateaus\/dips <https:\/\/github.com\/understand-covid\/epimodelingtoolkit\/blob\/master\/communication\/growth_rate_dips.ipynb> even when there is still exponential growth going on. Would appreciate any feedback.",
        "user": "U0104M5CWV9",
        "ts": "1586729788.009500",
        "team": "TUU48M0T1",
        "user_team": "TUU48M0T1",
        "source_team": "TUU48M0T1",
        "user_profile": {
            "avatar_hash": "g690df94d06f",
            "image_72": "https:\/\/secure.gravatar.com\/avatar\/690df94d06f11222b2bb4d765ee7ba52.jpg?s=72&d=https%3A%2F%2Fa.slack-edge.com%2Fdf10d%2Fimg%2Favatars%2Fava_0002-72.png",
            "first_name": "John",
            "real_name": "John Urbanik",
            "display_name": "John Urbanik",
            "team": "TUU48M0T1",
            "name": "johnurbanik",
            "is_restricted": false,
            "is_ultra_restricted": false
        },
        "attachments": [
            {
                "fallback": "[understand-covid\/epimodelingtoolkit] communication\/growth_rate_dips.ipynb",
                "text": "```\n{\n \"cells\": [\n  {\n   \"cell_type\": \"code\",\n   \"execution_count\": 341,\n   \"metadata\": {\n    \"jupyter\": {\n     \"source_hidden\": true\n    }\n   },\n   \"outputs\": [],\n   \"source\": [\n    \"import numpy as np\\n\",\n    \"import pandas as pd\\n\",\n    \"import matplotlib.pyplot as plt\"\n   ]\n  },\n  {\n   \"cell_type\": \"markdown\",\n   \"metadata\": {},\n   \"source\": [\n    \"# 'Plateaus' and 'Dips' in case numbers as an artifact of changing policies and partially isolated subpopulations\\n\",\n    \"\\n\",\n    \"Many simulations and pulibcations have talked about the possibility of 'peaks' in various places around the world. In effect, a peak is a time where the effective reproductive number R(t) &lt; 1. In this definition, it is a time after which the new case count \/ hospitalization count \/ death count is expected to asymptotically decrease to 0, unless behavior changes in a way that results in the effective reproductive number increasing again. \\n\",\n    \"\\n\",\n    \"However, many people have been optimistically looking for peaks by looking at the number of cases\/hospitalizations\/deaths and noticing that in several places, these seem to have plateaued or are decreasing.\\n\",\n    \"\\n\",\n    \"Unfortunately, a plateau\/dip of this sort has many explanations. Many people have talked about the possibility that people are dying in their homes or that testing rates are changing, but even controlling for these things, several mathematical effects that come out of exponential growth can result in these numbers ocurring while the effective reproduction number R(t) &gt; 1.\\n\",\n    \"\\n\",\n    \"A subset of these are discussed below:\\n\",\n    \"\\n\",\n    \"1. Quick changes in R(t) (i.e. policy) can result in plateaus or even large decreases in the daily count of new cases\/hospitalizations\/deaths temporarily.\\n\",\n    \"2. In the presence of heterogenous subpopulations where some reduce R(t) to &lt; 1, but others do not, similar dips and plateaus can arise when looking at the aggregated data. This holds at every scale where people don't mix completely randomly or people display different behaviors\\n\",\n    \"\\n\",\n    \"We simulate these effects with a simple SIR model, but more realistic compartmental models would also show these effects, and we believe that they may arrise in reality.\"\n   ]\n  },\n  {\n   \"cell_type\": \"markdown\",\n   \"metadata\": {},\n   \"source\": [\n    \"### Modeling of dips that are in response to policy changes that alter the effective reproductive number\\n\",\n    \"\\n\",\n    \"In the below model, a single homogenous population has it's R(t) change over time linearly for a few days after policy implementation, before settling at a new level. Additionally, we model a period where the R(t) does in fact go below 1 in order to show that the shapes can look locally similar.\"\n   ]\n  },\n  {\n   \"cell_type\": \"code\",\n   \"execution_count\": 401,\n   \"metadata\": {},\n   \"outputs\": [],\n   \"source\": [\n    \"susceptible = np.zeros(200)\\n\",\n    \"infected = np.zeros(200)\\n\",\n    \"resistant = np.zeros(200)\\n\",\n    \"susceptible[0] = 1e7\\n\",\n    \"infected[0] = 10\\n\",\n    \"infection_length = 14\\n\",\n    \"growth_rate = np.concatenate([\\n\",\n    \"    np.repeat(3.4, 20),\\n\",\n    \"    np.linspace(3.4, 2.2, 5),\\n\",\n    \"    np.repeat(2.2, 15),\\n\",\n    \"    np.linspace(2.2, 1.6, 5),\\n\",\n    \"    np.repeat(1.6, 15),\\n\",\n    \"    np.linspace(1.6, .9, 30),\\n\",\n    \"    np.repeat(.7, 200)\\n\",\n    \"])[:200]\\n\",\n    \"for v in range(len(susceptible)):\\n\",\n    \"    if v != 0:\\n\",\n    \"        susceptible[v] = susceptible[v - 1] - infected[v - 1] * growth_rate[v]\/infection_length\\n\",\n    \"        infected[v] = infected[v - 1] + infected[v - 1] * growth_rate[v]\/infection_length - infected[v - 1]\/infection_length\\n\",\n    \"        resistant[v] = resistant[v - 1] + infected[v - 1]\/infection_length\"\n   ]\n  },\n  {\n   \"cell_type\": \"code\",\n   \"execution_count\": 402,\n   \"metadata\": {\n    \"jupyter\": {\n     \"source_hidden\": true\n    }\n   },\n   \"outputs\": [\n    {\n     \"data\": {\n      \"image\/png\": \"iVBORw0KGgoAAAANSUhEUgAABHgAAAKrCAYAAACKiDr4AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+\/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjEsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+j8jraAAAgAElEQVR4nOzdeVxUVf8H8M8MIDOyrwKCuOCCIKCCYqaYJGqhaRlabj3l1vJUlpXlY5Za+ZiaZZZpllqWmv2ezLXSXModd0AUlVVAZd9mYJg5vz+ImyibxjBc\/LxfL18vZ+7cc793P\/fLOecqhBACREREREREREQkW0pTB0BERERERERERP8MEzxERERERERERDLHBA8RERERERERkcwxwUNEREREREREJHNM8BARERERERERyRwTPEREREREREREMscEDxFRE3Xt2jX0798fNjY2ePXVV\/HOO+9g3Lhxpg6rQQgh8K9\/\/QsODg7o1avXHc3btm1b7N6920iRGZ+fnx\/27dtntPK\/+OILvPzyyzVO\/\/nnnzFmzJg6y3nzzTexdOlSAMAff\/yBzp07N1iMtVmzZg3uv\/\/+RllWXRrrnEtKSoJCoUB5eTkAYOjQoVi7dq3Rlws0re1dH2fPnsV9991n6jDuOXK\/7hIR3SuY4CEiakR3UkleuXIlnJ2dUVBQgMWLFxs5ssb1559\/4rfffkNaWhqOHTtm6nAaVWxsLAYMGHBX8yoUClhZWcHa2hqtW7fGK6+8Ar1eL00vKyvD\/Pnz8dprrwG4PXEAAMOHD0dMTAzOnj1b43Ju3LiBdevWYerUqQCAfv364cKFC3cVc22qi4+AnTt3YuLEiaYOo0kKCAiAvb09tm7daupQiIiImhwmeIiImqjk5GR07doVCoXC1KE0uOTkZLRt2xZWVlamDkV2zpw5g6KiIuzfvx8bN27EV199JU3bsmULunTpgtatW9daxhNPPIGVK1fWOH3NmjV46KGHoFarGyxuYxBCwGAwmDoMamRjx47FF198YeowmgUmV4mImhcmeIiITKSya8SMGTPg4OCAdu3aYefOnQCAp556CmvXrsXChQthbW19W6ufffv2wdPTs8p3N7cOMhgMWLBgATp06AAnJydERUUhJycHwN+tJtauXYs2bdrA2dkZ7733nlSOXq\/H+++\/jw4dOsDGxgY9e\/ZEamoqACA+Ph6DBg2Co6MjOnfujE2bNtW4funp6Rg+fDgcHR3h4+ODVatWAQBWr16NSZMm4fDhw7C2tsacOXOqnX\/VqlXw9fWFjY0NunbtipMnT0rTTp8+jYCAANjZ2WH06NHQarUAgNzcXERGRsLFxQUODg6IjIxEWlqaNN+AAQMwe\/Zs9O3bFzY2NoiIiEBWVpY0fd26dfD29oaTkxPmzZtX722q1Woxbtw4ODk5wd7eHiEhIbh27Vq163Vzme+88w6ioqIwYcIE2NjYwM\/PD9HR0TVu05v5+Pigb9++OH36tPTdzp07ERYWJn3u378\/AMDe3h7W1tY4fPiwtB22b99eY9m3llPd8XazI0eO4L777oO9vT0CAwPr3QWtpvgAVHteVMY+a9Ys9O3bFy1btsSVK1dw6NAhhISEwM7ODiEhITh06JD0+1tbzd3a7aq2fQ5UtIqq7\/5RKBT45JNP0L59ezg7O+O1116TElAGgwHz58+Ht7c3XF1dMWHCBOTn51dbzoABA\/Dll19Kn6s7Fz788EM89thjVeb797\/\/XWP3vNTUVDz66KNwcXGBk5MTXnjhhSrTa9reX3\/9tbTs9u3bV0msVB4XixcvhqurK9zd3fH1119L07OzszFs2DDY2toiJCQE\/\/nPf6p0B6vterJjxw507doVNjY2aN26NRYtWlRl++zZswelpaXVrmtN1x7gzs+52NhYKcZWrVrh\/fffBwAcO3YMffr0gb29Pdzd3fHCCy+grKwMQEXicfr06XB1dYWdnR0CAgIQExMDACgtLcWMGTPQpk0btGrVCtOmTYNGowEAZGVlITIyEvb29nB0dES\/fv1qTGDWdqwBwFdffQVfX184ODhg8ODBSE5OrjLv8uXL0bFjR3Ts2LHa8r\/55hvpvLj5\/lDXuj\/\/\/PN49dVXq\/x+2LBhUndPIiIyMkFERI3G29tb\/Pbbb0IIIb7++mthbm4uVq5cKcrLy8Vnn30m3N3dhcFgEEIIMXHiRDFr1ixp3jlz5oixY8cKIYTYu3evaN26dY1lf\/TRR6J3794iNTVVaLVaMWXKFDFmzBghhBCJiYkCgJg0aZIoKSkRp0+fFi1atBBxcXFCCCEWLlwo\/P39RXx8vDAYDOL06dMiKytLFBUVCU9PT\/HVV18JnU4nTpw4IZycnERMTEy169q\/f3\/x7LPPCo1GI06dOiWcnZ3F7t27pXXv27dvjdtp06ZNwsPDQxw7dkwYDAaRkJAgkpKSpPUMCQkRV69eFdnZ2aJLly7i888\/F0IIkZWVJTZv3iyKi4tFQUGBGDVqlHjkkUekcsPCwkT79u3FhQsXRElJiQgLCxNvvPGGEEKI2NhYYWVlJf744w9RWloqXn31VWFubl6vbbpixQoRGRkpiouLRXl5uYiOjhb5+fl1HgNz5swRlpaWYvv27aK8vFzMnDlT9O7du8btAkAkJCQIIYQ4f\/68cHNzE0uWLJGmBwcHi02bNkmfK\/e1TqerUk52drYAUGOMzs7O4tixY9Ln6o63SmlpacLR0VFs375d6PV68euvvwpHR0dx\/fr1GtejtvjqOi\/CwsKEl5eXiImJETqdTmRmZgp7e3uxbt06odPpxHfffSfs7e1FVlaWEKLq9hai6nlU1z6\/m\/0zYMAAkZ2dLZKTk0XHjh3FqlWrhBBCrF69WnTo0EFcvnxZFBYWipEjR4px48ZVux3CwsKk+Wo6F9LT00XLli1Fbm6uEEIInU4nXFxcRHR09G1xlZeXi4CAAPHyyy+LoqIiodFoxB9\/\/FGv7b1t2zZx6dIlYTAYxL59+4RarRYnTpwQQlQcF2ZmZmL27NmirKxMbN++XajVapGTkyOEEGL06NFi9OjRori4WMTGxgpPT0\/pvK\/reuLm5iYOHDgghBAiJydHWmYlGxsbcebMmWr3Q23XnjvZpwUFBcLNzU0sWrRIaDQaUVBQII4cOSKEECI6OlocPnxY6HQ6kZiYKLp06SI++ugjIYQQu3btEj169BC5ubnCYDCIuLg4kZ6eLoQQ4qWXXhLDhg0T2dnZoqCgQERGRoqZM2cKIYSYOXOmmDp1qigrKxNlZWXiwIED0n64VW3H2v\/+9z\/RoUMHERcXJ3Q6nZg3b57o06dPlXkffPBBkZ2dLUpKSm4ru\/K82L9\/v9BqtWL69OnCzMxMOi9qW\/ejR48Kd3d3odfrhRBC3LhxQ6jVapGZmVntehARUcNigoeIqBHdmuDp0KGDNK24uFgAEBkZGUKIf5bg6dKli\/RAI4QQ6enpwtzcXKqQAxCpqanS9JCQEPH9998LIYTo1KmT+Omnn26LfcOGDeL++++v8t2UKVPEO++8c9tvU1JShFKpFAUFBdJ3M2fOFBMnTpTWvbYET0REhFi6dGm107y9vcU333wjfX7ttdfE1KlTq\/3tqVOnhL29vfQ5LCxMzJs3T\/q8fPlyMXjwYCGEEO+++66UsBGiYn9YWFjUa5uuXr1a9OnTp8YHzlvjvzmBEB4eLk2LjY0VKpWqxnkBCBsbG9GyZUsBQIwZM0ZotVppuo+Pj9i5c6f0uaYET1lZmQAgkpOTq12Oubm5OH\/+vPS5tgTPggULpERFpYiICLFmzZoa16O2+Oo6L8LCwsTs2bOl6evWrRMhISFVyg0NDRVff\/21EKL2BE9d+\/xu9s\/N23\/58uVi4MCBQgghBg4cKJYvXy5Ni4+Pv+2crC7BU9u5MGTIELFy5UohhBBbt24Vvr6+1f7u0KFDwtnZ+bbjQIi6t\/etHnnkESmevXv3CpVKVaVcFxcXcfjwYVFeXi7Mzc1FfHy8NG3WrFnSeV\/X9cTLy…",
                "title": "communication\/growth_rate_dips.ipynb",
                "footer": "<https:\/\/github.com\/understand-covid\/epimodelingtoolkit|understand-covid\/epimodelingtoolkit>",
                "id": 1,
                "title_link": "https:\/\/github.com\/understand-covid\/epimodelingtoolkit\/blob\/master\/communication\/growth_rate_dips.ipynb",
                "footer_icon": "https:\/\/github.githubassets.com\/favicon.ico",
                "color": "24292f",
                "mrkdwn_in": [
                    "text"
                ],
                "bot_id": "BUW789DM0",
                "app_unfurl_url": "https:\/\/github.com\/understand-covid\/epimodelingtoolkit\/blob\/master\/communication\/growth_rate_dips.ipynb",
                "is_app_unfurl": true
            }
        ],
        "blocks": [
            {
                "type": "rich_text",
                "block_id": "RsdyS",
                "elements": [
                    {
                        "type": "rich_text_section",
                        "elements": [
                            {
                                "type": "text",
                                "text": "Hey all. Have been noticing that a lot of media attention in the US is about the concept of 'peaks' and trying to forecast from data whether specific regions (Washington, NY, France, Spain) are actually experiencing them. I put together a quick simulation based analysis of how changes to the effective reproductive number can result in temporary plateaus\/dips "
                            },
                            {
                                "type": "link",
                                "url": "https:\/\/github.com\/understand-covid\/epimodelingtoolkit\/blob\/master\/communication\/growth_rate_dips.ipynb"
                            },
                            {
                                "type": "text",
                                "text": " even when there is still exponential growth going on. Would appreciate any feedback."
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        "client_msg_id": "5db45a6e-c357-4c89-8ffd-6de8960e6684",
        "type": "message",
        "text": "<@UV85V64SD> <@U010B412MF1> <https:\/\/arxiv.org\/abs\/2004.04463> very interested in your takes on this paper from Karl Friston (one of the top neuroscientists). They elect to use a mean field approximation and perform variational inference. My intuition is that they've formulated the model in a way that a few small adaptations could do a good job of capturing a lot of the heterogeneity that comes from network structure.",
        "user": "U0104M5CWV9",
        "ts": "1586736569.011700",
        "team": "TUU48M0T1",
        "user_team": "TUU48M0T1",
        "source_team": "TUU48M0T1",
        "user_profile": {
            "avatar_hash": "g690df94d06f",
            "image_72": "https:\/\/secure.gravatar.com\/avatar\/690df94d06f11222b2bb4d765ee7ba52.jpg?s=72&d=https%3A%2F%2Fa.slack-edge.com%2Fdf10d%2Fimg%2Favatars%2Fava_0002-72.png",
            "first_name": "John",
            "real_name": "John Urbanik",
            "display_name": "John Urbanik",
            "team": "TUU48M0T1",
            "name": "johnurbanik",
            "is_restricted": false,
            "is_ultra_restricted": false
        },
        "attachments": [
            {
                "service_name": "arXiv.org",
                "title": "Dynamic causal modelling of COVID-19",
                "title_link": "https:\/\/arxiv.org\/abs\/2004.04463",
                "text": "This technical report describes a dynamic causal model of the spread of coronavirus through a population. The model is based upon ensemble or population dynamics that generate outcomes, like new...",
                "fallback": "arXiv.org: Dynamic causal modelling of COVID-19",
                "from_url": "https:\/\/arxiv.org\/abs\/2004.04463",
                "service_icon": "https:\/\/static.arxiv.org\/static\/browse\/0.2.9\/images\/icons\/favicon.ico",
                "id": 1,
                "original_url": "https:\/\/arxiv.org\/abs\/2004.04463"
            }
        ],
        "blocks": [
            {
                "type": "rich_text",
                "block_id": "Bl4j",
                "elements": [
                    {
                        "type": "rich_text_section",
                        "elements": [
                            {
                                "type": "user",
                                "user_id": "UV85V64SD"
                            },
                            {
                                "type": "text",
                                "text": " "
                            },
                            {
                                "type": "user",
                                "user_id": "U010B412MF1"
                            },
                            {
                                "type": "text",
                                "text": " "
                            },
                            {
                                "type": "link",
                                "url": "https:\/\/arxiv.org\/abs\/2004.04463"
                            },
                            {
                                "type": "text",
                                "text": " very interested in your takes on this paper from Karl Friston (one of the top neuroscientists). They elect to use a mean field approximation and perform variational inference. My intuition is that they've formulated the model in a way that a few small adaptations could do a good job of capturing a lot of the heterogeneity that comes from network structure."
                            }
                        ]
                    }
                ]
            }
        ],
        "thread_ts": "1586736569.011700",
        "reply_count": 10,
        "reply_users_count": 3,
        "latest_reply": "1586885734.017000",
        "reply_users": [
            "UV85V64SD",
            "U010B412MF1",
            "U0104M5CWV9"
        ],
        "replies": [
            {
                "user": "UV85V64SD",
                "ts": "1586763056.012100"
            },
            {
                "user": "U010B412MF1",
                "ts": "1586792936.012300"
            },
            {
                "user": "U010B412MF1",
                "ts": "1586792958.012600"
            },
            {
                "user": "U0104M5CWV9",
                "ts": "1586792998.012800"
            },
            {
                "user": "U0104M5CWV9",
                "ts": "1586793021.013000"
            },
            {
                "user": "U0104M5CWV9",
                "ts": "1586793329.013300"
            },
            {
                "user": "UV85V64SD",
                "ts": "1586796441.013500"
            },
            {
                "user": "U010B412MF1",
                "ts": "1586885205.016500"
            },
            {
                "user": "U010B412MF1",
                "ts": "1586885241.016700"
            },
            {
                "user": "U0104M5CWV9",
                "ts": "1586885734.017000"
            }
        ],
        "subscribed": false
    }
]